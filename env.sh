#!/bin/bash

# a scripts for auto configure my working environment
# on ubuntu 
# feilengcui008@gmail.com

# basic path definition
PATH_ME=~/me 
PATH_ME_SOFTWARE=$PATH_ME/software
PATH_ME_MYCODE=$PATH_ME/mycode
PATH_ME_SOURCECODE=$PATH_ME/sourcecode
PATH_ME_WORKSPACE=$PATH_ME/workspace
PATH_ME_DEVTOOLS=$PATH_ME/devtools
PATH_ME_MYNOTES=$PATH_ME/mynotes
PATH_ME_MYMOOCS=$PATH_ME/mymoocs
mkdir -p $PATH_ME 
mkdir -p $PATH_ME_SOFTWARE 
mkdir -p $PATH_ME_WORKSPACE 
mkdir -p $PATH_ME_DEVTOOLS 
mkdir -p $PATH_ME_SOURCECODE 

# check os kernel 
OS=`uname | tr [A-Z] [a-z]`

# output color
if [ $OS = 'linux' ];then 
RED_COLOR='\E[1;31m'
GREEN_COLOR='\E[1;32m'
YELLOW_COLOR='\E[1;33m'
BLUE_COLOR='\E[1;34m'
END_COLOR='\E[0m'
#echo -e "${RED_COLOR}installing basic utilities...${END_COLOR}"
fi

# installer
INSTALLER=''
if [ $OS = 'linux' ];then 
  INSTALLER="sudo apt-get -y"
fi 
if [ $OS = 'darwin' ];then 
  INSTALLER="brew"
fi 

BASIC_TOOLS_TARGETS=""
BASIC_LIBS_TARGETS=""
VENDOR_LIBS_TARGETS=""
PIP_PACKAGES_TARGETS=""

######## Ubuntu ########
# install tools and libs for Ubuntu, get dotfiles
if [ $OS = 'linux' ];then
  # basic tools and libs definition for Ubuntu 
  BASIC_TOOLS_TARGETS="vim tmux ctags git gcc g++ clang autoconf automake cmake \
    aptitude python-pip"
  BASIC_LIBS_TARGETS="libboost-all-dev openssl libcrypto++-dev"
  VENDOR_LIBS_TARGETS="libgoogle-glog-dev libgflags-dev libgtest-dev libevent-dev \
    libeigen3-dev libjemalloc-devi libleveldb-dev libsnappy-dev thrift-compiler"
  PIP_PACKAGES_TARGETS="ipython requests tornado django"
  # update 
  INSTALLER update
fi

######## MacOS ########
# install tools and libs for MacOS, get dotfiles
if [ $OS = 'darwin' ];then
  # first install homebrew 
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  # basic tools and libs definition for MacOS
  BASIC_TOOLS_TARGETS="vim ctags git autoconf automake cmake python"
  BASIC_LIBS_TARGETS="boost openssl cryptopp"
  VENDOR_LIBS_TARGETS="glog gflags libevent eigen jemalloc leveldb snappy protobuf \
    thrift"
  PIP_PACKAGES_TARGETS="ipython requests tornado django"
fi

for i in $BASIC_TOOLS_TARGETS;do
  echo -e "${RED_COLOR}============ installing $i... ============${END_COLOR}"
  $INSTALLER install $i
done

for i in $BASIC_LIBS_TARGETS;do
  echo -e "${RED_COLOR}============ installing $i... ============${END_COLOR}"
  $INSTALLER install $i
done

for i in $VENDOR_LIBS_TARGETS;do
  echo -e "${RED_COLOR}============ installing $i... ============${END_COLOR}"
  $INSTALLER install $i
done

for i in $PIP_PACKAGES_TARGETS;do 
  echo -e "${RED_COLOR}============ installing $i... ============${END_COLOR}"
  if [ $OS = 'linux' ];then
    /usr/bin/sudo pip install $i
  fi 
  if [ $OS = 'darwin' ];then 
    pip install $i
  fi
done

# pull dotfiles from github
cd /tmp 
echo -e "${RED_COLOR}============ clone dotfiles ============${END_COLOR}"
git clone https://bitbucket.org/feilengcui008/dotfiles
if [ $OS = 'linux' ];then 
  cp dotfiles/dotfiles.tar.gz ~/
  cd ~/ && tar xvf dotfiles.tar.gz && rm dotfiles.tar.gz 
fi 
if [ $OS = 'darwin' ];then 
  cp dotfiles/mac_dotfiles.tar.gz ~/
  cd ~/ && tar xvf mac_dotfiles.tar.gz && rm mac_dotfiles.tar.gz 
fi

# pull my private repos 
#cd $PATH_ME 
#echo -e "${RED_COLOR}============ clone mycode ============${END_COLOR}"
#git clone https://bitbucket.org/feilengcui008/mycode 
