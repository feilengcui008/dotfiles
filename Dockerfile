FROM  ubuntu:latest

MAINTAINER feilengcui008 <feilengcui008@gmail.com>

RUN sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list

RUN apt-get update 

RUN apt-get -y install wget sudo

WORKDIR /tmp

RUN /usr/bin/wget https://raw.githubusercontent.com/feilengcui008/env/master/env.sh 

RUN /bin/bash env.sh
